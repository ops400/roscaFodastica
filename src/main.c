#include <raylib.h>
#include <stdio.h>
#define WW 1200
#define WH 720

const char* WNAME = "rosca fodastica";

int main(int argc, char** argv){
    InitWindow(WW, WH, WNAME);
    SetTargetFPS(60);
    Model rosca = LoadModel("./src/res/rosca.obj");
    Vector3 roscaPos = {-20.0f, 0.0f, 0.0f};
    Vector3 roscaScale = {10.0f, 10.0f, 10.0f};
    Vector3 roscaPos2 = {10.0f, 0.0f, 0.0f};
    float roscaRotationAngleSpeed = 1.0f;
    float roscaRotationAngle = roscaRotationAngleSpeed;

    Camera cam = {0};
    cam.position = (Vector3){-5.0f, 0.0f, 42.0f};
    cam.up = (Vector3){0.0f, 1.0f, 0.0f};
    cam.target = (Vector3){0.0f, 1.0f, 0.0f};
    cam.projection = CAMERA_PERSPECTIVE;
    cam.fovy = 90.0f;

    BoundingBox bound = GetMeshBoundingBox(rosca.meshes[0]);

    // DisableCursor(); isso e para prender o cursor na janela
    while(!WindowShouldClose()){
        // UpdateCamera(&cam, CAMERA_FIRST_PERSON); isso e para controlar a camera com o mouse
        //y e z? era para ter acento, mas o gcc nao gosta
        if(IsKeyDown(KEY_A)) cam.position.x -= 0.5f; 
        if(IsKeyDown(KEY_D)) cam.position.x += 0.5f; 
        if(IsKeyDown(KEY_W)) cam.position.y -= 0.5f; 
        if(IsKeyDown(KEY_S)) cam.position.y += 0.5f; 
        if(IsKeyDown(KEY_Q)) cam.position.z -= 0.5f; 
        if(IsKeyDown(KEY_E)) cam.position.z += 0.5f; 
        //os conrrole tao uma bosta
        // printf("cam.position.x=%f\ncam.position.y=%f\ncam.position.z=%f\n",cam.position.x, cam.position.y, cam.position.z);
        // if(IsKeyDown(KEY_UP)) roscaRotationAngleTest += 1.0f; bosta
        // if(IsKeyDown(KEY_DOWN)) roscaRotationAngleTest -= 1.0f; bosta
        // printf("roscaratation=%f\n", roscaRotationAngleTest);
        // mais controle bosta
        if(IsKeyDown(KEY_UP)) ++roscaRotationAngleSpeed;
        if(IsKeyDown(KEY_DOWN)) --roscaRotationAngleSpeed;
        roscaRotationAngle += roscaRotationAngleSpeed;
        if(IsKeyPressed(KEY_ESCAPE)) CloseWindow(); //apenas emergencias!
        BeginDrawing();
        BeginMode3D(cam);
        ClearBackground(BLACK);
        DrawModelWiresEx(rosca, roscaPos2, (Vector3){1.0f, 1.0f, 1.0f}, roscaRotationAngle, roscaScale, GREEN);
        DrawModelEx(rosca, roscaPos, (Vector3){1.0f, 1.0f, 1.0f}, roscaRotationAngle, roscaScale, GRAY);
        // DrawGrid(10, 10.0f); fazia um quadriculado de teste
        EndMode3D();
        DrawText("A setinha para cima/\\ aumentar a velocidade e a setinha para baixo\\/ diminui a velocidade, as setinhas do teclado", 
        0, 
        0, 
        20, 
        WHITE);
        DrawText("Cuidado a velocidade vai abaixo de zero então acaba que não tem muita diferença", 
        0, 
        20, 
        20, 
        WHITE);
        DrawText("ESC sai", 
        0, 
        40, 
        20, 
        RED);
        DrawFPS(0, 60);
        EndDrawing();
    }
    UnloadModel(rosca);
    CloseWindow();

    return 0;
}